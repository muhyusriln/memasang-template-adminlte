<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PostController extends Controller
{
    public function create() {
        return view('posts.create');
    }

    public function store(Request $request) {
        // dd($request->all());
        $query = DB::table('films')->insert([
            "judul" => $request["judul"],
            "ringkasan" => $request["ringkasan"],
            "tahun" => $request["tahun"]
        ]);

        return redirect('/posts/create');
    }
}
